# Maintainer: Daniel M. Capella <polyzen@archlinux.org>
# Maintainer: Johannes Löthberg <johannes@kyriasis.com>

_gitname=pacman-contrib
pkgname=pacman-contrib-jr-git
pkgver=1.5.3.r7.g7f6c19f
pkgrel=1
pkgdesc='Contributed scripts and tools for pacman systems with patches'
url=https://gitlab.archlinux.org/pacman/pacman-contrib
arch=('x86_64')
license=('GPL')
depends=('fakeroot' 'pacman' 'perl')
makedepends=('asciidoc' 'git')
optdepends=('findutils: for pacdiff --find'
            'mlocate: for pacdiff --locate'
            'sudo: privilege elevation for several scripts'
            'vim: default merge program for pacdiff')
provides=("$_gitname")
conflicts=("$_gitname")
source=('git+https://gitlab.archlinux.org/pacman/pacman-contrib.git'
        'colorize.patch')
sha512sums=('SKIP'
            'e9c3f9a91d3cbcabe1a40838b2934edd71dd827fb4dd44c704270fdba31b5b613c48e5600c80e7bf70550b32fd0c669df00dc24c7c9a4b1dabcbde73c056a1e4')

pkgver() {
  cd $_gitname
  git describe --long --tags | sed -r 's/^v//;s/-/.r/;s/-/./'
}

prepare() {
  cd $_gitname
  patch --strip=1 --input="${srcdir}/colorize.patch"
  ./autogen.sh
}

build() {
  cd $_gitname
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --enable-git-version
  make
}

check() {
  make -C $_gitname check
}

package() {
  cd $_gitname
  make DESTDIR="$pkgdir" install
}

